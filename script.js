//1.Які існують типи даних у Javascript?
//1.String
//2.Number
//3.Boolean
//4.Null
//5.Object
//6.BigInt
//7.Symbol
//8.Undefined

//2.У чому різниця між == і ===?
//=== це строге рівняння, тобто порівнюватись данні повинні одного типу та значення щоб отримати true, а == спочатку перетворює данні - потім порювнюють.  

//3.Що таке оператор?
//Оператор - це певна інструкція для обробки данних. Пр використанні того чи іншого оператора ми чекаємо на певний результат обробки данних.  


//Завдання:

let userName = prompt("What is your name?", "")
let userAge = prompt("What is your age?", "")

while (userName === null || userName.length === 0) {
    userName = prompt("What is your name?", userName);
}

while (userAge === null || userAge.length === 0 || !Number(userAge)) { 
    userAge = prompt("What is your age?", userAge);
}

userAge=+userAge;

if (userAge<18) {
    alert("You are not allowed to visit this website");
} else if (userAge>=18 && userAge<=22) {
    let areYouSure=confirm ("Are you sure you want to continue?");
    console.log(areYouSure);

    if (areYouSure==false) {
        alert("You are not allowed to visit this website");
    } else {
        alert(`Welcome, ` + userName);
    }
} else {
    alert(`Welcome, ` + userName);
}
